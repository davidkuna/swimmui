import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import common from "./reducers/common";
import sidePanel from "./reducers/sidePanel";
import map from "./reducers/map";

export default combineReducers({
  common,
  sidePanel,
  map,
  router: routerReducer
});
