import { SIDE_PANEL_CLOSE, SIDE_PANEL_OPEN } from "../constants/actionTypes";

export function toggleSidePanel() {
  return (dispatch, getState) => {
    if (getState().sidePanel.opened) {
      dispatch({
        type: SIDE_PANEL_CLOSE
      });
    } else {
      dispatch({
        type: SIDE_PANEL_OPEN
      });
    }
  };
}
