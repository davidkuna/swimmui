import axios from "axios";
import { BoundsConvertor } from "../helpers/mapHelper";
import {
  MAP_FETCH_PLACES_PENDING,
  MAP_FETCH_PLACES_FULFILLED,
  MAP_FETCH_PLACES_REJECTED,
  MAP_CHANGE_CENTER,
  MAP_HIGHTLIGHT_ITEM,
  MAP_UNHIGHLIGHT_ITEM
} from "../constants/actionTypes";

export function fetchPlaces() {
  var CancelToken = axios.CancelToken;
  var call = CancelToken.source();
  var calling = false;

  return (dispatch, getState) => {
    if (!getState().map.mounted) return;

    if (calling) {
      call.cancel("Operation canceled by the user.");
    }
    calling = true;
    call = CancelToken.source();
    dispatch({ type: MAP_FETCH_PLACES_PENDING });
    axios
      .post(
        getState().common.api.url + "filter/places",
        {
          bounds: BoundsConvertor.toSwimmBounds(getState().map.map.getBounds())
        },
        {
          cancelToken: call.token
        }
      )
      .then(res => {
        dispatch({ type: MAP_FETCH_PLACES_FULFILLED, payload: res.data.data });
      })
      .catch(function(thrown) {
        if (!axios.isCancel(thrown)) {
          console.log("Request error", thrown.message);
          dispatch({ type: MAP_FETCH_PLACES_REJECTED, payload: thrown });
        }
      });
  };
}

export function setGoogleMapCenter(latitude, longitude, zoom) {
  console.log(latitude, longitude);
  return {
    type: MAP_CHANGE_CENTER,
    zoom,
    center: { lat: latitude, lng: longitude }
  };
}

export function highlightItem(item) {
  return {
    type: MAP_HIGHTLIGHT_ITEM,
    payload: item
  };
}

export function unhighlightItem(item) {
  return {
    type: MAP_UNHIGHLIGHT_ITEM,
    payload: item
  };
}
