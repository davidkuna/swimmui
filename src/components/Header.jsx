import React, { Component } from "react";
import "../css/Header.css";

class Header extends Component {
  render() {
    return (
      <nav className="Header navbar navbar-light bg-light">
        <span className="navbar-brand">
          <img
            src="https://picsum.photos/30"
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt=""
          />
          Swimm UI
        </span>
      </nav>
    );
  }
}

export default Header;
