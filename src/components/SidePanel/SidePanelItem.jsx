import React from "react";

const SidePanelItem = props => {
  return (
    <div
      className={
        "list-group-item list-group-item-action flex-column align-items-start" +
        (props.active ? " active" : "")
      }
      onClick={props.onClickHandler.bind(this, props.item)}
      onMouseEnter={props.onMouseEnterHandler.bind(this, props.item)}
      onMouseLeave={props.onMouseLeaveHandler.bind(this, props.item)}
    >
      <div className="d-flex w-100 justify-content-between">
        <h5 className="mb-1">{props.item.name}</h5>
        <small>{props.item.shortcut}</small>
      </div>
      <p className="mb-1" />
      <small>
        {props.item.latitude} {props.item.longitude}
      </small>
    </div>
  );
};

export default SidePanelItem;
