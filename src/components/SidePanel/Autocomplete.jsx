import React, { Component } from "react";
import axios from "axios";
import "../../css/Autocomplete.css";

const doneTypingInterval = 300;

var typingTimer = 0;

class Autocomplete extends Component {
  state = {
    apiUrl: this.props.api.url + "filter/autocomplete?term=",
    items: [],
    value: "",
    show: false
  };

  componentDidMount() {
    document.body.addEventListener("click", this.handleBodyClick.bind(this));
  }

  componentWillUnmount() {
    document.body.removeEventListener("click", this.handleBodyClick);
  }

  handleBodyClick() {
    this.setState({ show: false });
  }

  handleOnClick() {
    if (this.state.items.length) {
      this.setState({ show: true });
    }
  }

  handleOnKeyUp(e) {
    clearTimeout(typingTimer);
    var value = e.target.value;
    var parent = this;
    typingTimer = setTimeout(function() {
      parent.onDoneTypingHandler(value);
    }, doneTypingInterval);
  }

  handleOnKeyDown() {
    clearTimeout(typingTimer);
    if (this.state.items.length === 0) {
      this.setState({ show: false });
    }
  }

  onDoneTypingHandler(value) {
    axios
      .get(this.state.apiUrl + value)
      .then(res => {
        this.setState({ items: res.data.data, show: true });
      })
      .catch(error => {
        console.log("onDoneTypingHandler error: " + error);
      });
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  onItemClickHandler = item => {
    this.setState({ value: item.name, show: false });
    this.props.onPlaceChanged(item);
  };

  render() {
    return (
      <div className="form-group" id="Autocomplete">
        <label htmlFor="place">Město, adresa nebo PSČ</label>
        <input
          type="text"
          name="place"
          className="form-control"
          id="place"
          value={this.state.value}
          autoComplete="off"
          onKeyUp={this.handleOnKeyUp.bind(this)}
          onKeyDown={this.handleOnKeyDown.bind(this)}
          onChange={this.handleChange.bind(this)}
          onClick={this.handleOnClick.bind(this)}
        />

        <div
          className={
            this.state.show ? "dropdown-menu visible" : "dropdown-menu"
          }
        >
          {this.state.items.map(item => (
            <span
              className="dropdown-item"
              key={item.id}
              onClick={this.onItemClickHandler.bind(this, item)}
            >
              {item.name}
            </span>
          ))}

          {this.state.items.length === 0 && (
            <span>
              Hleadné město <strong>{this.state.value}</strong> nebylo nalezeno
            </span>
          )}
        </div>
      </div>
    );
  }
}

export default Autocomplete;
