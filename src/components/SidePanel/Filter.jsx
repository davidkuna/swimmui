import React, { Component } from "react";
import { connect } from "react-redux";
import Autocomplete from "./Autocomplete";
import "../../css/Filter.css";
import { setGoogleMapCenter } from "../../actions/mapActions";

class Filter extends Component {
  render() {
    return (
      <form
        id="Filter"
        onSubmit={e => {
          e.preventDefault();
          return false;
        }}
      >
        <Autocomplete
          api={this.props.api}
          onPlaceChanged={this.props.onPlaceChanged}
        />
        <button type="submit" className="btn btn-primary">
          Vyhledat
        </button>
      </form>
    );
  }
}

const mapStateToProps = store => {
  return {
    api: store.common.api
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPlaceChanged: place =>
      dispatch(setGoogleMapCenter(place.latitude, place.longitude, 12))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filter);
