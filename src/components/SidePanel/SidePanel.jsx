import React, { Component } from "react";
import SidePanelItem from "./SidePanelItem";
import Filter from "./Filter";
import "../../assets/icomoon/style.css";
import "../../css/SidePanel.css";
import { connect } from "react-redux";
import { toggleSidePanel } from "../../actions/sidePanelActions";
import {
  setGoogleMapCenter,
  highlightItem,
  unhighlightItem
} from "../../actions/mapActions";

class SidePanel extends Component {
  render() {
    return (
      <div
        className={"SidePanel" + (this.props.opened ? " opened" : " closed")}
      >
        <div className="toggle-button" onClick={this.props.toggleSidePanel}>
          <span
            className={
              "icon icon-circle-" + (this.props.opened ? "right" : "left")
            }
          />
        </div>
        <div className="container">
          <Filter />
          <div className="list-group">
            {this.props.items.map(item => (
              <SidePanelItem
                key={item.id}
                item={item}
                onClickHandler={this.props.itemClick}
                onMouseEnterHandler={this.props.highlightItem}
                onMouseLeaveHandler={this.props.unhighlightItem}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    opened: store.sidePanel.opened,
    items: store.map.markers
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleSidePanel: () => dispatch(toggleSidePanel()),
    itemClick: item =>
      dispatch(setGoogleMapCenter(item.latitude, item.longitude, 17)),
    highlightItem: item => dispatch(highlightItem(item)),
    unhighlightItem: item => dispatch(unhighlightItem(item))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SidePanel);
