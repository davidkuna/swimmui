import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Header from "./Header";
import Home from "./Home/index";

import { APP_LOAD, REDIRECT } from "../constants/actionTypes";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    appLoaded: state.common.appLoaded
  };
};

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) =>
    dispatch({ type: APP_LOAD, payload, token, skipTracking: true }),
  onRedirect: () => dispatch({ type: REDIRECT })
});

class App extends Component {
  state = {};
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
