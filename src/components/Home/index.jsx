import React, { Component } from "react";
import { connect } from "react-redux";
import SwimmMap from "../SwimmMap/SwimmMap";
import SidePanel from "../SidePanel/SidePanel";
import "../../css/home-page.css";

class Home extends Component {
  render() {
    return (
      <div className="home-page">
        <div
          className={"SwimmMap" + (this.props.fullSizeMap ? " full-size" : "")}
        >
          <SwimmMap />
        </div>
        <SidePanel />
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    fullSizeMap: !store.sidePanel.opened
  };
};

export default connect(
  mapStateToProps,
  () => {
    return {};
  }
)(Home);
