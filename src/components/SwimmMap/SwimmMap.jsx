import React from "react";
import { connect } from "react-redux";
import { compose, withProps, withHandlers } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";
import "../../css/SwimmMap.css";
import { MAP_MOUNTED } from "../../constants/actionTypes";
import { fetchPlaces } from "../../actions/mapActions";

const mapStateToProps = store => {
  return { map: store.map };
};

const mapDispatchToProps = dispatch => {
  return {
    mapMounted: ref => {
      dispatch({ type: MAP_MOUNTED, payload: ref });
    },
    boundsChanged: () => {
      dispatch(fetchPlaces());
    }
  };
};

const SwimmMap = compose(
  withProps(props => {
    return {
      containerElement: <div style={{ height: `100%` }} />,
      mapElement: <div style={{ height: `100%` }} />,
      loadingElement: <div style={{ height: `100%` }} />,
      googleMapURL: props.map.googleMapURL
    };
  }),
  withHandlers(props => {
    var boundsChangeTimer = 0;
    var boundsChangeInterval = 1000;
    return {
      onMapMounted: () => ref => {
        props.mapMounted(ref);
      },
      onBoundsChanged: () => () => {
        clearTimeout(boundsChangeTimer);
        boundsChangeTimer = setTimeout(function() {
          props.boundsChanged();
        }, boundsChangeInterval);
      }
    };
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    defaultZoom={props.map.defaultZoom}
    defaultCenter={props.map.defaultCenter}
    center={props.map.center || props.map.defaultCenter}
    zoom={props.map.zoom || props.map.defaultZoom}
    defaultOptions={{ gestureHandling: "greedy" }}
    ref={props.onMapMounted}
    onBoundsChanged={props.onBoundsChanged}
  >
    {props.map.markers.map(marker => (
      <Marker
        key={marker.id}
        position={{ lat: marker.latitude, lng: marker.longitude }}
        animation={marker.animation || null}
      />
    ))}
  </GoogleMap>
));

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SwimmMap);
