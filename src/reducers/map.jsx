/*global google*/

import {
  MAP_MOUNTED,
  MAP_FETCH_PLACES_FULFILLED,
  MAP_CHANGE_CENTER,
  MAP_HIGHTLIGHT_ITEM,
  MAP_UNHIGHLIGHT_ITEM
} from "../constants/actionTypes";

import { BOUNCE } from "google-map-react";

const defaultState = {
  map: null,
  mounted: false,
  defaultCenter: { lat: 49.189556, lng: 16.597843 },
  defaultZoom: 7,
  zoom: null,
  center: null,
  googleMapURL:
    "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBma_54drXzC73M5AYgNaiPCVkJtIkOzms&libraries=geometry,drawing,places",
  markers: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case MAP_MOUNTED: {
      return { ...state, map: action.payload, mounted: true };
    }

    case MAP_FETCH_PLACES_FULFILLED: {
      return { ...state, markers: action.payload };
    }

    case MAP_CHANGE_CENTER: {
      return {
        ...state,
        zoom: action.zoom,
        center: action.center
      };
    }

    case MAP_UNHIGHLIGHT_ITEM:
    case MAP_HIGHTLIGHT_ITEM: {
      return {
        ...state,
        markers: state.markers.map(
          marker =>
            marker.id === action.payload.id
              ? {
                  ...marker,
                  animation:
                    action.type === MAP_HIGHTLIGHT_ITEM
                      ? google.maps.Animation.BOUNCE
                      : null
                }
              : marker
        )
      };
    }

    default:
      return state;
  }
};
