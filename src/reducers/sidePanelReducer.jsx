import { APP_LOAD, REDIRECT } from "../constants/actionTypes";

const defaultState = {
  opened: true
};

export default (state = defaultState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
