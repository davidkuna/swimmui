import { SIDE_PANEL_OPEN, SIDE_PANEL_CLOSE } from "../constants/actionTypes";

const defaultState = {
  opened: true
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case SIDE_PANEL_OPEN: {
      return { ...state, opened: true };
    }

    case SIDE_PANEL_CLOSE: {
      return { ...state, opened: false };
    }

    default:
      return state;
  }
};
