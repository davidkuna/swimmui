export const APP_LOAD = "APP_LOAD";
export const REDIRECT = "REDIRECT";
export const ASYNC_START = "ASYNC_START";
export const ASYNC_END = "ASYNC_END";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const REGISTER = "REGISTER";

export const SIDE_PANEL_OPEN = "SIDE_PANEL_OPEN";
export const SIDE_PANEL_CLOSE = "SIDE_PANEL_CLOSE";

export const MAP_MOUNTED = "MAP_MOUNTED";
export const MAP_BOUNDS_CHANGED = "MAP_BOUNDS_CHANGED";
export const MAP_FETCH_PLACES_PENDING = "MAP_FETCH_PLACES_PENDING";
export const MAP_FETCH_PLACES_FULFILLED = "MAP_FETCH_PLACES_FULFILLED";
export const MAP_FETCH_PLACES_REJECTED = "MAP_FETCH_PLACES_REJECTED";
export const MAP_CHANGE_CENTER = "MAP_CHANGE_CENTER";
export const MAP_HIGHTLIGHT_ITEM = "MAP_HIGHTLIGHT_ITEM";
export const MAP_UNHIGHLIGHT_ITEM = "MAP_UNHIGHLIGHT_ITEM";
