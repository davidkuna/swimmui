export class BoundsConvertor {
  static toSwimmBounds(bounds) {
    return {
      north_east: {
        lat: bounds.f.f,
        lng: bounds.b.f
      },
      south_west: {
        lat: bounds.f.b,
        lng: bounds.b.b
      }
    };
  }
}
