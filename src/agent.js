let token = null;
const tokenPlugin = req => {
  if (token) {
    req.set("authorization", `Token ${token}`);
  }
};

export default {
  tokenPlugin,
  setToken: _token => {
    token = _token;
  }
};
